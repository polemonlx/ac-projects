package org.academiadecodigo.tailormoons.webServer;

import java.io.IOException;

public class Server {

    public static void main(String[] args) {

        Listener listener;

        try {
            listener = new Listener(45000);
        } catch (IOException ex) {
            System.out.println("Can't open the server on this port.");
            return;
        }

        listener.start();

    }

}
