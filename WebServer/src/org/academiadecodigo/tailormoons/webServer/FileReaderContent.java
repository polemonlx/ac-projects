package org.academiadecodigo.tailormoons.webServer;

import java.io.*;

public class FileReaderContent {


    private static FileInputStream fileInputStream;

    public static void getFileContent(String path, OutputStream out) throws IOException {

        File file = new File(path);
        if (!file.exists() || file.isDirectory()) {
            return;
        }

        FileInputStream fileInputStream = new FileInputStream(path);

        byte[] buffer = new byte[2048];
        int numberOfBytes;
        while ((numberOfBytes = fileInputStream.read(buffer)) != -1) {
            out.write(buffer, 0, numberOfBytes);
        }

        fileInputStream.close();

    }


    public static int getFileSize(String path) throws FileNotFoundException {
        File file = new File(path);
        if (!file.exists() || file.isDirectory()) {
            throw new FileNotFoundException();
        }
        return (int) file.length();
    }

}
