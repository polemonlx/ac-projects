package org.academiadecodigo.tailormoons.webServer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

public class ResponseBuilder {

    private String fileName;
    private String extension;

    public ResponseBuilder(String request) {
        String[] headerParts = request.split(" ");
        if (headerParts.length < 3) {
            throw new IllegalArgumentException();
        }

        String[] values = headerParts[1].split("\\.");
        this.fileName = values[0];
        if (this.fileName.equals("/")) {
            this.fileName = "/index";
            this.extension = "html";
            return;
        }
        if (values.length < 2) {
            return;
        }
        this.extension = values[1];

    }


    public byte[] getHeader() {

        int size;
        try {
            size = FileReaderContent.getFileSize("www" + fileName + ((!extension.equals("")) ? "." + extension : ""));

        } catch (FileNotFoundException ex) {
            System.out.println("No file found!");
            return ("HTTP/1.0 404 Not Found\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length: 0 \r\n" +
                    "\r\n").getBytes();
        }

        if (extension.equals("html") || extension.equals("")) {
            return ("HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length: " + size + " \r\n" +
                    "\r\n").getBytes();
        }

        if (extension.equals("css")) {
            return ("HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: text/html; charset=UTF-8\r\n" +
                    "Content-Length: " + size + " \r\n" +
                    "\r\n").getBytes();
        }

        if (extension.equals("png") || extension.equals("jpg")) {
            return ("HTTP/1.0 200 Document Follows\r\n" +
                    "Content-Type: image/" + extension + " \r\n" +
                    "Content-Length: " + size + " \r\n" +
                    "\r\n").getBytes();
        }

        return null;

    }


    public void getContent(OutputStream out) throws IOException {
        FileReaderContent.getFileContent("www" + fileName + ((!extension.equals("")) ? "." + extension : ""), out);
    }

}