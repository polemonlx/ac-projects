package org.academiadecodigo.bootcamp.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class KeyboardListener implements KeyboardHandler {

    Picture picture;

    public KeyboardListener(Picture picture) {
        this.picture = picture;
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                picture.translate(-1, 0);
                break;
            case KeyboardEvent.KEY_RIGHT:
                picture.translate(1, 0);
                break;
            case KeyboardEvent.KEY_UP:
                picture.translate(0, -1);
                break;
            case KeyboardEvent.KEY_DOWN:
                picture.translate(0, 1);
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
