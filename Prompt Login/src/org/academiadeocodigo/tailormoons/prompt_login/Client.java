package org.academiadeocodigo.tailormoons.prompt_login;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadeocodigo.tailormoons.prompt_login.login.Login;

public class Client {

    private final Prompt prompt = new Prompt(System.in, System.out);


    public void start() {

        Login login = new Login();
        String user = login.requestLogin(prompt);

        System.out.println("Your are now logged as " + user);
    }

}
