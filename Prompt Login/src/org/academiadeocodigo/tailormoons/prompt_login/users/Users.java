package org.academiadeocodigo.tailormoons.prompt_login.users;

import java.util.HashMap;
import java.util.Map;

public class Users {

    private static final Map<String, String> credentials = new HashMap<>() {
        {
            put("diogo", "123");
            put("admin", "admin");
        }
    };


    public static boolean verifyPassword(String name, String password) {
        if (!credentials.containsKey(name)) {
            return false;
        }

        return credentials.get(name).equals(password);
    }

}
