package org.academiadeocodigo.tailormoons.prompt_login.login;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import org.academiadeocodigo.tailormoons.prompt_login.users.Users;

public class Login {

    public String requestLogin(Prompt prompt) {

        while (true) {

            StringInputScanner nameScanner = new StringInputScanner();
            nameScanner.setMessage("Username: ");

            PasswordInputScanner pwScanner = new PasswordInputScanner();
            pwScanner.setMessage("Password: ");

            String name = prompt.getUserInput(nameScanner);
            String pw = prompt.getUserInput(pwScanner);

            if (Users.verifyPassword(name, pw)) {
                return name;
            }

            System.out.println("Wrong user name, try again. \n");

        }

    }

}
