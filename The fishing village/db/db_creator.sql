/* Erase database "boat_booking" */
DROP DATABASE IF EXISTS boat_booking;

/* Create new database "boat_booking" */
CREATE DATABASE boat_booking;

/* Swap to database "boat_booking" */
USE boat_booking;

/* Create tables */
CREATE TABLE sailors(id INTEGER NOT NULL UNIQUE, name CHAR(15) NOT NULL, age INTEGER NOT NULL, PRIMARY KEY (id));

CREATE TABLE boats(id INTEGER NOT NULL UNIQUE, name CHAR(15) NOT NULL, color CHAR(15) NOT NULL, PRIMARY KEY (id));

CREATE TABLE reservations(id_sailor INTEGER NOT NULL, id_boat INTEGER NOT NULL, day DATE NOT NULL, PRIMARY KEY(id_sailor,id_boat), FOREIGN KEY (id_sailor) REFERENCES sailors(id), FOREIGN KEY (id_boat) REFERENCES boats(id));

/* Insert values in tables */
INSERT INTO sailors VALUES(0,'Diogo',25);
INSERT INTO sailors VALUES(1,'Sofia',30);
INSERT INTO sailors VALUES(2,'André',35);
INSERT INTO sailors VALUES(3,'Nuno',53);
INSERT INTO sailors VALUES(6,'Outro gajo',20);

INSERT INTO boats VALUES(0,'Afonso','red');
INSERT INTO boats VALUES(1,'Navegador','blue');
INSERT INTO boats VALUES(2,'Carolina','pink');

/* Get info from tables */
SELECT name FROM boats;

SELECT * FROM boats WHERE color = 'red';

SELECT * FROM sailors WHERE (age > 20 AND age < 30);

SELECT id,name FROM sailors WHERE (age < 50 AND id > 5);