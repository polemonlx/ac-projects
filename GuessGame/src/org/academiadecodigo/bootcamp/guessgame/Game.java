package org.academiadecodigo.bootcamp.guessgame;

import java.util.Scanner;

public class Game {

    public static int maxRange;
    public static int[] guessed;

    public void startGame(Player[] player) {

        Scanner scanner = new Scanner(System.in);

        while (true) {
            int number = Generator.generateNumber(maxRange);

            int guess = -1;

            System.out.println("NUMBER: " + number);
            System.out.println("");

            int counter = 0;
            while (guess != number) {
                for (int j = 0; j < player.length; j++) {
                    System.out.print(counter + " ::: Player " + player[j].getName() + " choose a number: ");

                    guess = player[j].chooseNumber(counter);
                    System.out.println(guess);
                    guessed[counter] = guess;
                    counter++;

                    if (guess == number) {
                        System.out.println("YEAY MADAFAKA! Player " + player[j].getName() + " guessed right");
                        break;
                    }
                }
            }
            scanner.nextLine();
        }

    }

    public static void setMaxRange(int range) {
        maxRange = range;
    }

    public static void startingValues() {
        guessed = new int[Game.maxRange];
        for (int i = 0; i < Game.maxRange; i++) {
            guessed[i] = -1;
        }
    }

}