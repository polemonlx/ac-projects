package org.academiadecodigo.bootcamp.guessgame;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        System.out.print("Max number to guess: ");
        int maxRange = scanner.nextInt();

        Game.setMaxRange(maxRange);
        Game.startingValues();


        Player[] player = new Player[3];
        for (int i = 0; i < 3; i++) {

            System.out.println("Player " + i + " name: ");
            String name = scanner.next();

            player[i] = new Player(name);
            player[i].startingValues();
        }


        Game game1 = new Game();
        game1.startGame(player);

    }

}
