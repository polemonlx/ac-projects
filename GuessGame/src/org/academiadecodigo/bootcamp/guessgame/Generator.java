package org.academiadecodigo.bootcamp.guessgame;

public class Generator {

    public static int generateNumber(int max) {
        int number = (int) (Math.random() * max);
        return number;
    }

}
