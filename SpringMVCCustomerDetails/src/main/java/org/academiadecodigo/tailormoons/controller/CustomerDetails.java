package org.academiadecodigo.tailormoons.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CustomerDetails {

    @RequestMapping(method = RequestMethod.GET, value = "/customerDetails")
    public String displayCustomer(Model model) {

        model.addAttribute("greeting", "Hello World");

        return "index";
    }

}
