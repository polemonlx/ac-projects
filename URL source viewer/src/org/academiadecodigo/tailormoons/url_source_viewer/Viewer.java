package org.academiadecodigo.tailormoons.url_source_viewer;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class Viewer {

    private BufferedInputStream in;

    public Viewer(String hostName) throws IOException {
        URL url = new URL(hostName);
        URLConnection urlConnection = url.openConnection();
        in = new BufferedInputStream(urlConnection.getInputStream());
    }


    public void start() {

        String text;
        byte[] buffer = new byte[1024];
        int bytesRead;

        try {

            while ((bytesRead = in.read(buffer)) != -1) {
                text = new String(buffer, 0, bytesRead);
                System.out.println(text);
            }

        } catch (IOException ex) {
            System.out.println("Can't read the stream properly. Try to repeat.");
        } finally {
            close(in);
        }

    }


    private void close(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException ex) {
            System.out.println("Can't close the stream");
        }
    }

}
