package org.academiadecodigo.tailormoons.exceptions.exception;

public class FileException extends Exception {

    public FileException(String message) {
        super(message);
    }

}
