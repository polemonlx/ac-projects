package org.academiadecodigo.tailormoon.io;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        FileInputStream fileToCopy = null;
        FileOutputStream fileToPaste = null;

        try {
            fileToCopy = new FileInputStream("textToBeCopied");
            fileToPaste = new FileOutputStream("textToBePaste");

            byte[] buffer = new byte[5];
            int numberOfBytes;

            while ((numberOfBytes = fileToCopy.read(buffer)) != -1) {
                fileToPaste.write(buffer, 0, numberOfBytes);
            }


        } catch (IOException ex) {
            ex.printStackTrace();

        } finally {
            close(fileToCopy);
            close(fileToPaste);
        }

    }

    private static void close(Closeable stream) {
        try {
            if (stream == null) {
                return;
            }
            stream.close();
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }

}
