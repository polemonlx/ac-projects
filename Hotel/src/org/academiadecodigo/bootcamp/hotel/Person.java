package org.academiadecodigo.bootcamp.hotel;

public class Person {

    private int room;
    private Hotel hotel;

    public Person(Hotel hotel) {
        this.hotel = hotel;
    }

    private void setRoom(int room) {
        this.room = room;
    }

    private int getRoom() {
        return room;
    }

    public void goHotel() {
        room = hotel.checkIn();
    }

    public void leaveHotel() {
        hotel.checkOut(getRoom());
        room = -1;
    }

}
