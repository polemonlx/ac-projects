package org.academiadecodigo.tailormoons.bit_mapping.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import javax.swing.plaf.basic.BasicSplitPaneUI;

public class KeyboardListener implements KeyboardHandler {

    private static final int[] KEY_CODES = {
            KeyboardEvent.KEY_UP,
            KeyboardEvent.KEY_DOWN,
            KeyboardEvent.KEY_LEFT,
            KeyboardEvent.KEY_RIGHT,
            KeyboardEvent.KEY_SPACE,
            KeyboardEvent.KEY_C, //Clear screen
            KeyboardEvent.KEY_S, //Save screen
            KeyboardEvent.KEY_L, //Load save
            KeyboardEvent.KEY_D, //Delete save
            KeyboardEvent.KEY_Q  //Quit game
    };

    private final Interactive entity;

    public KeyboardListener(Interactive entity) {
        this.entity = entity;
        init();
    }


    private void init() {
        Keyboard keyboard = new Keyboard(this);

        for (int code : KEY_CODES) {
            for (KeyboardEventType type : KeyboardEventType.values()) {
                subscribe(keyboard, code, type);
            }
        }
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            entity.setPressed(true);
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP ||
                keyboardEvent.getKey() == KeyboardEvent.KEY_DOWN ||
                keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT ||
                keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT) {
            entity.setMoving(keyboardEvent.getKey(),true);
        }

        entity.keyPressed(keyboardEvent.getKey());
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            entity.setPressed(false);
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP ||
                keyboardEvent.getKey() == KeyboardEvent.KEY_DOWN ||
                keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT ||
                keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT) {
            entity.setMoving(keyboardEvent.getKey(),false);
        }
    }


    private void subscribe(Keyboard keyboard, int code, KeyboardEventType type) {
        KeyboardEvent event = new KeyboardEvent();
        event.setKey(code);
        event.setKeyboardEventType(type);
        keyboard.addEventListener(event);
    }

}
