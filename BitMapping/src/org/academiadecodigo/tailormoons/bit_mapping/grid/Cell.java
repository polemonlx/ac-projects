package org.academiadecodigo.tailormoons.bit_mapping.grid;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {

    private final Rectangle rectangle;

    public Cell(int posX, int posY) {
        rectangle = new Rectangle(posX * Grid.CELL_SIZE + Grid.PADDING, posY * Grid.CELL_SIZE + Grid.PADDING, Grid.CELL_SIZE, Grid.CELL_SIZE);
        rectangle.draw();
    }


    public void setColor(ColorEnum color) {
        if (color == ColorEnum.NONE) {
            rectangle.draw();
            return;
        }

        rectangle.setColor(color.getColor());
        rectangle.fill();
    }


    public boolean isPainted() {
        return rectangle.isFilled();
    }

}
