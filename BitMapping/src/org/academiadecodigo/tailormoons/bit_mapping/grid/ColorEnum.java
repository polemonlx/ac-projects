package org.academiadecodigo.tailormoons.bit_mapping.grid;


import org.academiadecodigo.simplegraphics.graphics.Color;

public enum ColorEnum {
    RED(Color.RED),
    GREEN(Color.GREEN),
    BLUE(Color.BLUE),
    WHITE(Color.WHITE),
    LIGHT_GRAY(Color.LIGHT_GRAY),
    GRAY(Color.GRAY),
    DARK_GRAY(Color.DARK_GRAY),
    BLACK(Color.BLACK),
    CYAN(Color.CYAN),
    MAGENTA(Color.MAGENTA),
    YELLOW(Color.YELLOW),
    PINK(Color.PINK),
    ORANGE(Color.ORANGE),

    NONE(Color.WHITE);

    private Color color;

    ColorEnum(Color color) {
        this.color = color;
    }


    public Color getColor() {
        return color;
    }
}
