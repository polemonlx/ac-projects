package org.academiadecodigo.tailormoons.bit_mapping.grid;


public class Grid {

    public static final int CELL_SIZE = 20;
    public static final int PADDING = 10;

    private Cell[][] cells;

    private final int gridWidth;
    private final int gridHeight;

    public Grid(int gridWidth, int gridHeight) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
    }


    public void init() {

        cells = new Cell[gridWidth][gridHeight];

        for (int w = 0; w < gridWidth; w++) {
            for (int h = 0; h < gridHeight; h++) {

                cells[w][h] = new Cell(w, h);

            }
        }

    }


    @Override
    public String toString() {
        String text = "";

        for (int w = 0; w < gridWidth; w++) {
            for (int h = 0; h < gridHeight; h++) {

                if (cells[w][h].isPainted()) {
                    text = text + w + "," + h + "\n";
                }

            }
        }
        return text;
    }


    public void drawCell(int x, int y, ColorEnum colorEnum) {
        cells[x][y].setColor(colorEnum);
    }


    public void clear() {
        for (int w = 0; w < gridWidth; w++) {
            for (int h = 0; h < gridHeight; h++) {

                if (cells[w][h].isPainted()) {
                    cells[w][h].setColor(ColorEnum.NONE);
                }

            }
        }
    }


    public void loadGrid(String load) {
        for (String line : load.split("\n")) {
            String[] coordinates = line.split(",");
            drawCell(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]), ColorEnum.BLACK);
        }
    }

}
