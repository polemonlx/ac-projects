package org.academiadecodigo.bootcamp.battle.fighter;

public class Gladiator extends Fighter {

    private static final int attackDamage = 15;
    private static final int spellDamage = 10;
    private static final int health = 100;


    public Gladiator(String name) {
        super(name, attackDamage, spellDamage, health);
    }


    @Override
    public void hit(Fighter fighter) {

        int random = (int) (Math.random() * 2);

        if (random == 1) {
            //attack
            System.out.println(super.getName() + " inflicted " + attackDamage + " damage.");
            fighter.suffer(attackDamage);
            return;
        }

        //don't attack
        System.out.println("Inflicted " + attackDamage * 2 + " damage. CRITICAL STRIKE");
        fighter.suffer(attackDamage * 2);

    }

}
