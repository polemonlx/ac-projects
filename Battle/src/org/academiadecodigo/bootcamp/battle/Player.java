package org.academiadecodigo.bootcamp.battle;

import org.academiadecodigo.bootcamp.battle.fighter.Fighter;

public class Player {

    private final String name;
    private final Fighter[] fighters;

    public Player(String name, Fighter[] fighters) {
        this.name = name;
        this.fighters = fighters;
    }


    public void attack(Player player) {

        int ownFighter = (int) (Math.random() * fighters.length);
        while (fighters[ownFighter].isDead()) {
            ownFighter = (int) (Math.random() * fighters.length);
        }

        int enemyFighter = (int) (Math.random() * fighters.length);
        while (player.fighters[enemyFighter].isDead()) {
            enemyFighter = (int) (Math.random() * fighters.length);
        }


        int attack = (int) (Math.random() * 2);
        if (attack == 1) {
            //hit
            System.out.println(fighters[ownFighter].getName() + " from " + this.name + " try to hit " + fighters[enemyFighter].getName() + " from " + player.getName());
            fighters[ownFighter].hit(player.fighters[enemyFighter]);
            return;
        }

        //cast
        System.out.println(fighters[ownFighter].getName() + " from " + this.name + " try to cast " + fighters[enemyFighter].getName() + " from " + player.getName());
        fighters[ownFighter].cast(player.fighters[enemyFighter]);

    }


    public boolean hasLost() {

        for (Fighter fighter : fighters) {
            if (!fighter.isDead()) {
                return false;
            }
        }

        return true;
    }


    public String getName() {
        return name;
    }

}
