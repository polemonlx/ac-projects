package org.academiadecodigo.bootcamp.battle;

import org.academiadecodigo.bootcamp.battle.fighter.Fighter;
import org.academiadecodigo.bootcamp.battle.fighter.Gladiator;
import org.academiadecodigo.bootcamp.battle.fighter.Troll;
import org.academiadecodigo.bootcamp.battle.fighter.Wizard;

public class Main {

    public static void main(String[] args) {

        Player player1 = new Player("Player 1", new Fighter[]{new Troll("Troll"), new Gladiator("Gladiator"), new Wizard("Wizard")});
        Player player2 = new Player("Player 2", new Fighter[]{new Troll("Troll"), new Gladiator("Gladiator"), new Wizard("Wizard")});


        Arena arena = new Arena(player1, player2);
        arena.battle();

    }

}
