package org.academiadecodigo.tailormoons.todolist;

public class Main {

    public static void main(String[] args) {

        TodoList todoList = new TodoList();

        todoList.add(Importance.MEDIUM, 2, "Medium,2");
        todoList.add(Importance.HIGH, 2, "High,2");
        todoList.add(Importance.HIGH, 1, "High,1");
        todoList.add(Importance.LOW, 1, "Low,1");

        while(!todoList.isEmpty()) {
            System.out.println(todoList.remove());
        }

    }

}
