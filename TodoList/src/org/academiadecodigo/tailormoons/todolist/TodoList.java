package org.academiadecodigo.tailormoons.todolist;

import java.util.PriorityQueue;

public class TodoList {

    private PriorityQueue<Task> queue = new PriorityQueue<>();


    public void add(Importance importance, int priority, String text) {
        queue.offer(new Task(importance, priority, text));
    }


    public boolean isEmpty() {
        return queue.size() == 0;
    }


    public String remove() throws NullPointerException {
        if (queue.peek() == null) {
            throw new NullPointerException();
        }
        return queue.poll().getText();
    }

}
