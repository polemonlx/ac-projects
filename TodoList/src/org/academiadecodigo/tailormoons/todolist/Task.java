package org.academiadecodigo.tailormoons.todolist;

public class Task implements Comparable<Task> {

    private Importance importance;
    private int priority;
    private String text;

    public Task(Importance importance, int priority, String text) {
        this.importance = importance;
        this.priority = priority;
        this.text = text;
    }


    @Override
    public int compareTo(Task o) {

        if (this.importance.getValue() > o.importance.getValue()) {
            return 1;

        } else if (this.importance.getValue() < o.importance.getValue()) {
            return -1;

        } else {

            if (this.priority > o.priority) {
                return 1;

            } else if (this.priority < o.priority) {
                return -1;

            } else {
                return 0;

            }
        }
    }


    public String getText() {
        return text;
    }
}
