package org.academiadecodigo.carcrash.field;

public class Position {

    private int col;
    private int row;


    public int getCol() {
        return col;
    }


    public int getRow() {
        return row;
    }


    public void setCol(int col) {
        this.col = col;
    }


    public void setRow(int row) {
        this.row = row;
    }


    public boolean confirmMove(int moveX, int moveY, int posX, int posY) {

        int finalPosX = moveX + posX;
        int finalPosY = moveY + posY;

        if (finalPosX < 0 || finalPosX >= Field.getWidth()) {
            return false;
        }

        if (finalPosY < 0 || finalPosY >= Field.getHeight()) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return col + ", " + row;
    }
}
