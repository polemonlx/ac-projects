package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.Game;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {

    private static Position[] positions = new Position[Game.MANUFACTURED_CARS];


    public static Car getNewCar() {
        switch (CarType.values()[(int)(Math.random()*4)]) {
            case FIAT:
                return new Fiat(generatePos());

            case MUSTANG:
                return new Mustang(generatePos());

            case TANK:
                return new Tank(generatePos());

            case HELICOPTER:
                return new Helicopter(generatePos());
        }
        return new Fiat(generatePos());
    }


    private static Position generatePos() {

        int X = 0;
        int Y = 0;
        int counter = 0;

        boolean confirm = false;
        while (!confirm) {
            X = (int) (1 + Math.random() * Field.getWidth());
            Y = (int) (1 + Math.random() * Field.getHeight());

            confirm = true;
            for (Position position : positions) {
                counter++;
                if (position == null) {
                    break;
                }

                if (X == position.getCol() && Y == position.getRow()) {
                    confirm = false;
                    counter = 0;
                    break;
                }
            }

        }

        positions[counter] = new Position();
        positions[counter].setCol(X);
        positions[counter].setRow(Y);
        return positions[counter];

    }
}
