package org.academiadecodigo.carcrash.cars;

public enum CarType {
    FIAT(100),
    MUSTANG(250),
    TANK(50),
    HELICOPTER(350);


    private final int speed;

    CarType(int speed) {
        this.speed = speed;
    }


    public int getSpeed() {
        return speed;
    }

}
