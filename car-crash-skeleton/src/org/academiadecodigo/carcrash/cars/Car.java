package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.Direction;
import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    /**
     * The position of the car on the grid
     */
    private final Position pos;
    private final String name;
    private boolean crashed = false;
    private Direction direction;
    private final CarType carType;


    public Car(Position position, String name, CarType carType) {
        this.pos = position;
        this.name = name;
        this.carType = carType;
        direction = Direction.values()[(int) (Math.random() * 4)];
    }


    public Position getPos() {
        return pos;
    }


    public boolean isCrashed() {
        return crashed;
    }


    @Override
    public String toString() {
        return (!isCrashed()) ? name : "C";
    }


    public void setPos(int posX, int posY) {
        pos.setCol(posX);
        pos.setRow(posY);
    }


    public void setCrashed(boolean crashed) {
        this.crashed = crashed;
    }


    public boolean hasCollision(Car car) {
        return pos.toString().equals(car.getPos().toString());
    }


    public void collisionDetector(Car[] cars) {
        for (Car car : cars) {
            if (this.hashCode() == car.hashCode()) {
                continue;
            }
            if (this.hasCollision(car)) {
                this.setCrashed(true);
                car.setCrashed(true);
            }
        }
    }


    public Direction move() {

        int random = (int) (Math.random() * 3);

        if (random == 0) {
            verifyMove();
            return direction;
        }

        if (getPos().confirmMove(direction.moveX, direction.moveY, pos.getCol(), pos.getRow())) {
            return direction;
        }

        verifyMove();
        return direction;
    }


    public void verifyMove() {

        boolean success = false;
        while (!success) {
            direction = Direction.values()[(int) (Math.random() * 4)];
            if (getPos().confirmMove(direction.moveX, direction.moveY, pos.getCol(), pos.getRow())) {
                success = true;
            }
        }

    }


    public CarType getCarType() {
        return carType;
    }
}
