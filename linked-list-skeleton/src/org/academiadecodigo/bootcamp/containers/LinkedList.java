package org.academiadecodigo.bootcamp.containers;

public class LinkedList {

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     *
     * @param data the element to add
     */
    public void add(Object data) {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null) {
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    /**
     * Obtains an element by index
     *
     * @param index the index of the element
     * @return the element
     */
    public Object get(int index) {

        if (index >= length) {
            return null;
        }

        Node iterator = head.getNext();
        for (int i = 0; i < index; i++) {
            iterator = iterator.getNext();
        }
        return iterator.getData();

    }

    /**
     * Returns the index of the element in the list
     *
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(Object data) {

        if (length == 0) {
            return -1;
        }

        int index = 0;
        Node iterator = head.getNext();
        while (!iterator.getData().equals(data)) {

            if (iterator.getNext() == null) {
                return -1;
            }

            iterator = iterator.getNext();
            index++;
        }

        return index;
    }

    /**
     * Removes an element from the list
     *
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean remove(Object data) {

        if (length == 0) {
            return false;
        }

        Node lastNode = head;
        Node iterator = head.getNext();
        while (!iterator.getData().equals(data)) {

            if (iterator.getNext() == null) {
                return false;
            }
            lastNode = iterator;
            iterator = iterator.getNext();
        }

        lastNode.setNext(iterator.getNext());
        length--;

        return true;
    }

    private class Node {

        private Object data;
        private Node next;

        public Node(Object data) {
            this.data = data;
            next = null;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

}
