package org.academiadecodigo.tailormoons.scum;

public interface BiOperation<T> {

    void operation(T obj1, T obj2);

}
