package org.academiadecodigo.tailormoons.scum;

public class Main {

    public static void main(String[] args) {

        monoOperation("Diogo", obj -> System.out.println(obj + "."));

        biOperation("Diogo <3 ", "Java", (obj1, obj2) -> System.out.println(obj1 + obj2));

        biOperation(1, 2, ((obj1, obj2) -> {
            int result = obj1 + obj2;
            System.out.println(result);
        }));

    }


    public static <T> void monoOperation(T obj, MonoOperation<T> monoOperation) {
        monoOperation.operation(obj);
    }


    public static <T> void biOperation(T obj1, T obj2, BiOperation<T> biOperation) {
        biOperation.operation(obj1, obj2);
    }

}
