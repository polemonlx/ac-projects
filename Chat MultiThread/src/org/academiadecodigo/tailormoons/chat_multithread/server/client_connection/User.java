package org.academiadecodigo.tailormoons.chat_multithread.server.client_connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class User implements Runnable {

    private final Socket clientSocket;
    private final ClientManager clientManager;
    private String name;
    private boolean isAdmin;

    public User(Socket clientSocket, ClientManager clientManager) {
        this.clientSocket = clientSocket;
        this.clientManager = clientManager;
    }


    @Override
    public void run() {

        this.name = "Cadet " + clientManager.getCounter();
        clientManager.add(this);

        BufferedReader bufferedReader;
        String line;

        while (!clientSocket.isClosed()) {
            try {

                bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                line = bufferedReader.readLine();
                System.out.println(line);

                if (line == null) {
                    closeClient();
                    continue;
                }


                clientManager.receive(line, this);

            } catch (IOException exception) {
                exception.printStackTrace();
            }

        }

    }


    public boolean isAdmin() {
        return isAdmin;
    }


    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public Socket getClientSocket() {
        return clientSocket;
    }


    public void closeClient() {
        try {
            clientSocket.close();
            clientManager.removeUser(this);
        } catch (IOException ex) {
            System.out.println("Error closing client. " + ex.toString());
        }
    }

}
