package org.academiadecodigo.tailormoons.chat_multithread.server.command;

import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.ClientManager;
import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.User;

public class CommandList implements CommandHandler {
    @Override
    public void run(User user, ClientManager clientManager, String text) {
        
        clientManager.sendMessageUser("List of users: ", user);
        for (User userClient : clientManager.getUsers()) {
            clientManager.sendMessageUser(userClient.getName(), user);
        }

    }
}
