package org.academiadecodigo.tailormoons.chat_multithread.server.command;

import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.ClientManager;
import org.academiadecodigo.tailormoons.chat_multithread.server.client_connection.User;

public class CommandHelp implements CommandHandler {
    @Override
    public void run(User user, ClientManager clientManager, String text) {
        clientManager.sendMessageUser("/quit", user);
        clientManager.sendMessageUser("/name <name>", user);
        clientManager.sendMessageUser("/list", user);
        clientManager.sendMessageUser("/admin <password>", user);
        clientManager.sendMessageUser("/close", user);
    }
}
