package org.academiadecodigo.tailormoons.chat_multithread.client.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Receiver implements Runnable {

    private final BufferedReader in;
    private final Socket clientSocket;


    public Receiver(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }


    @Override
    public void run() {

        String message;

        while (!clientSocket.isClosed()) {
            try {
                message = in.readLine();
                System.out.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
