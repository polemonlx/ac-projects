package org.academiadecodigo.tailormoons.tcpChat.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerListening {

    private final int port = 6000;

    private final Socket clientSocket;

    public ServerListening() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();
    }


    public void start() {

        while (!clientSocket.isClosed()) {

            try {
                //GET MESSAGE FROM CLIENT
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String receiveText = in.readLine();

                //ANSWER TO CLIENT
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                out.println("Your string was: " + receiveText);

            } catch (IOException ex) {
                System.out.println("Error creating streams: " + ex.toString());
                return;
            }

        }
    }

}
