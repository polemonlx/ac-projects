package org.academiadecodigo.tailormoons.tcpChat.server;

import java.io.IOException;

public class ServerMain {

    public static void main(String[] args) {

        ServerListening serverListening;

        try {
            serverListening = new ServerListening();
        } catch (IOException ex) {
            System.out.println("Error creating socket: " + ex.toString());
            return;
        }

        serverListening.start();

    }

}
