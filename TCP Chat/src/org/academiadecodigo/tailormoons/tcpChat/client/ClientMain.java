package org.academiadecodigo.tailormoons.tcpChat.client;

import java.io.IOException;

public class ClientMain {

    private final static String USAGE_MESSAGE = "java ClientMain <hostName> <port>";

    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println(USAGE_MESSAGE);
            return;
        }

        String hostName = args[0];

        int portNumber;
        try {
            portNumber = Integer.parseInt(args[1]);
        } catch (IllegalArgumentException ex) {
            System.out.println("Port is not a valid number! " + USAGE_MESSAGE);
            return;
        }


        Client client;

        try {
            client = new Client(hostName, portNumber);

        } catch (IOException ex) {
            System.out.println("Error creating the socket. " + ex.toString());
            return;
        }

        client.start();

    }

}
