/* Erase database if exists */
DROP DATABASE IF EXISTS local_library;

/* Create database */
CREATE DATABASE local_library;

/* Swap to database */
USE local_library;

/* Create tables */
CREATE TABLE clients(
    id INTEGER NOT NULL UNIQUE, 
    name CHAR(15) NOT NULL, 

    PRIMARY KEY (id)
    );

CREATE TABLE books(
    id INTEGER NOT NULL UNIQUE, 
    name CHAR(15) NOT NULL, 
    publisher CHAR(15) NOT NULL, 
    reserved BIT DEFAULT 0, 
    client_id INTEGER, 

    PRIMARY KEY(id), 
    FOREIGN KEY (client_id) REFERENCES clients(id)
    );

CREATE TABLE loans(
    client_id INTEGER NOT NULL,
    book_id INTEGER NOT NULL,
    year INTEGER NOT NULL,

    PRIMARY KEY (client_id,book_id),
    FOREIGN KEY (client_id) REFERENCES clients(id),
    FOREIGN KEY (book_id) REFERENCES books(id)
);

/* Populate database */
INSERT clients VALUES (0,'client0');
INSERT clients VALUES (1,'client1');
INSERT clients VALUES (2,'client2');
INSERT clients VALUES (3,'client3');
INSERT clients VALUES (4,'client4');

INSERT books(id,name,publisher,reserved,client_id) VALUES(0,'book0','tolstoi',1,3);
INSERT books(id,name,publisher,reserved,client_id) VALUES(1,'book1','publisher1',0,NULL);
INSERT books(id,name,publisher,reserved,client_id) VALUES(2,'book2','publisher2',1,2);
INSERT books(id,name,publisher,reserved,client_id) VALUES(3,'book3','publisher3',1,4);
INSERT books(id,name,publisher,reserved,client_id) VALUES(4,'O Primo Basílio','publisher4',0,NULL);
INSERT books(id,name,publisher,reserved,client_id) VALUES(5,'book5','publisher5',0,NULL);

INSERT loans VALUES(1,3,1995);
INSERT loans VALUES(2,5,1920);
INSERT loans VALUES(4,2,2000);

/* Get values from exercise */
SELECT b.name AS 'Do we have any books by Tolstoi?'
FROM books AS b
WHERE b.publisher = 'tolstoi';

SELECT COUNT(b.id) AS 'How many books are out on loan?'
FROM books AS b
WHERE b.reserved = 'true';

SELECT DISTINCT b.publisher AS 'Which company published O Primo Basílio?'
FROM books AS b
WHERE b.name = 'O Primo Basílio';

SELECT c.name AS 'Which users have borrowed books published before 1974?'
FROM clients AS c, loans AS l
WHERE c.id = l.client_id AND l.year < 1974;