package org.academiadecodigo.bootcamp.AccountManager;

public class Account {

    private double balance;

    public Account(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    private boolean setBalance(double money) {
        boolean success = false;
        if (money <= balance) {
            balance -= money;
            success = true;
        }
        return success;
    }

    public double withdraw(double money, int pin) {

        if (!Bank.verify(pin,0)) {
            System.out.println("Wrong pin");
            return 0;
        }

        if (setBalance(money)) {
            System.out.println("Success!");
        } else {
            money = 0;
            System.out.println("Not enough money!");
        }
        System.out.println();
        return money;
    }

    public double deposit(double money) {
        balance += money;
        return balance;
    }

}
