package org.academiadecodigo.bootcamp.AccountManager;

public class Bank {

    private static int[] pins = new int[10];

    public static int generate(int id) {
        int pin = (int) (1000 + (Math.random() * 9000));
        pins[id] = pin;
        return pin;
    }

    public static boolean verify(int pin, int id) {
        if (pin == pins[id]) {
            return true;
        }
        return false;
    }

}
