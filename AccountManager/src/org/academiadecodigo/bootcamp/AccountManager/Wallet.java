package org.academiadecodigo.bootcamp.AccountManager;

public class Wallet {

    private double balance;

    public Wallet(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    private boolean setBalance(double money) {
        if (money <= balance) {
            balance -= money;
            return true;
        }

        return false;
    }

    public double withdraw(double money) {
        if (!setBalance(money)) {
            System.out.println("Not enough money!");
            return 0;
        }

        System.out.println("Success!");
        return money;
    }

    public double deposit(double money) {
        balance += money;
        return balance;
    }

}
