package org.academiadecodigo.bootcamp.AccountManager;

import java.util.Scanner;

public class Manager {

    private final Wallet[] wallets;
    private final Account[] accounts;
    private final Person[] persons;

    public Manager(Wallet[] wallets, Account[] accounts, Person[] persons) {
        this.wallets = wallets;
        this.accounts = accounts;
        this.persons = persons;
    }

    public void startManagement() {
        Scanner scanner = new Scanner(System.in);

        boolean keepGoing = true;
        while (keepGoing) {

            displayMenu();
            String nextMove = scanner.nextLine();

            if (nextMove.equals("stop")) {
                keepGoing = false;
                continue;
            }

            takeDecision(nextMove);

        }

    }

    private void takeDecision(String nextMove) {

        String[] commands = nextMove.split(" ");

        double money = 0;
        if (!commands[0].equals("print") && !commands[0].equals("buy")) {
            money = Double.parseDouble(commands[1].toString());
        }

        switch (commands[0]) {
            case "receive":
                persons[0].depositWallet(money);
                break;

            case "get":
                persons[0].withdrawAccount(money);
                break;

            case "save":
                persons[0].saveToAccount(money);
                break;

            case "buy":
                displayList();
                Scanner scanner = new Scanner(System.in);
                String stuff = scanner.nextLine();
                boolean success = persons[0].payFromWallet(Products.valueOf(stuff).getPrice());
                if (success){
                    Products.valueOf(stuff).setAmount(1);
                }
                break;

            case "print":
                if (commands[1].equals("W")) {
                    System.out.println(wallets[0].getBalance());
                } else if (commands[1].equals("A")) {
                    System.out.println(accounts[0].getBalance());
                }
                break;
        }

    }

    private void displayList() {
        System.out.println();
        for(Products product: Products.values()) {
            System.out.println(product + "          Stock: " + product.getAmount() + "      Price: " + product.getPrice());
        }
    }

    private void displayMenu() {
        System.out.println();
        System.out.println("Next move?");
        System.out.println("receive [double money] - Receive money");
        System.out.println("get [double money] ----- Withdraw money from account");
        System.out.println("save [double money] ---- Save money to account");
        System.out.println("buy -------------------- buy something from a list (from wallet)");
        System.out.println("print [W/A] ------------ Print money from wallet or account");
        System.out.println("stop -------------------- Finish all tasks");
    }

}
