package org.academiadecodigo.bootcamp.AccountManager;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("How much you start your account?");
        double startMoney = scanner.nextDouble();

        String nextMove = scanner.nextLine();

        Wallet[] wallets = new Wallet[1];
        Account[] accounts = new Account[1];
        Person[] persons = new Person[1];

        wallets[0] = new Wallet(0);
        accounts[0] = new Account(startMoney);
        persons[0] = new Person(wallets[0], accounts[0], Bank.generate(0));

        Manager manager = new Manager(wallets, accounts, persons);

        manager.startManagement();

    }

}
