package org.academiadecodigo.tailormoons;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomerDetails extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = new User();
        user.setName("Diogo");
        user.setPhoneNumber("914795703");

        req.setAttribute("user", user);

        RequestDispatcher page1Dispatcher = getServletContext().getRequestDispatcher("/resources/index.jsp");

        page1Dispatcher.forward(req, resp);

    }

}
