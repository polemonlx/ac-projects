package org.academiadecodigo.bootcamp.rockpaperscissor;

public class Generator {

    public static HandType generate() {
        int number = (int) (Math.random() * (HandType.values().length));

        switch (number) {
            case 0:
                return HandType.ROCK;
            case 1:
                return HandType.PAPER;
            case 2:
                return HandType.SCISSORS;
            case 3:
                return HandType.LIZARD;
            default:
                return HandType.SPOCK;
        }

    }

}
