package org.academiadecodigo.bootcamp.rockpaperscissor;

public class Game {

    private int roundsNumber;
    private static int GamesCount = 1;

    public Game(int roundsNumber) {
        Game.GamesCount++;
        this.roundsNumber = roundsNumber;
    }

    public void startGame(Player[] players) {
        for (int i = 0; i < roundsNumber; i++) {
            startRound(players);
        }

        System.out.println("Player " + players[0].getName() + " won: " + players[0].getRoundWins() + "   lost: " + players[0].getRoundLosses());
        System.out.println("Player " + players[1].getName() + " won: " + players[1].getRoundWins() + "   lost: " + players[1].getRoundLosses());

        if (players[0].getRoundWins() > players[1].getRoundWins()) {
            System.out.println(players[0].getName() + " won the game!");
            players[0].setWins();
            players[1].setLosses();

        } else if (players[0].getRoundWins() < players[1].getRoundWins()) {
            System.out.println(players[1].getName() + " won the game!");
            players[1].setWins();
            players[0].setLosses();

        } else {
            System.out.println("yeaaahhh..... well, no one won......... ");

        }

        players[0].resetRounds();
        players[1].resetRounds();

    }

    private void startRound(Player[] players) {
        /*HandType[] handTypes = new HandType[2];
        for (Player player : players) {
            System.out.print(player.getName() + "Choose your hand: ");

        }*/

        System.out.print(players[0].getName() + " choose your hand: ");
        HandType handPlayer1 = players[0].showHand();
        System.out.println(handPlayer1.getName());

        System.out.print(players[1].getName() + " choose your hand: ");
        HandType handPlayer2 = players[1].showHand();
        System.out.println(handPlayer2.getName());

        if (handPlayer2.getName() == handPlayer1.getWinAgainst1() || handPlayer2.getName() == handPlayer1.getWinAgainst2()) {
            // Player 1 wins
            players[0].setRoundWin();
            players[1].setRoundLoss();
            System.out.println("Player " + players[0].getName() + " win -> (" + players[0].getRoundWins() + "-" + players[1].getRoundWins() + ")");

        } else if (handPlayer1.getName() == handPlayer2.getWinAgainst1() || handPlayer1.getName() == handPlayer2.getWinAgainst2()) {
            // Player 2 wins
            players[1].setRoundWin();
            players[0].setRoundLoss();
            System.out.println("Player " + players[1].getName() + " win -> (" + players[0].getRoundWins() + "-" + players[1].getRoundWins() + ")");

        } else {
            // Players tie (nothing happens.. #Sad_story)
            System.out.println("No one wins, the show must go on!");

        }
        System.out.println();

    }

    public static int getGamesCount() {
        return Game.GamesCount;
    }

}
