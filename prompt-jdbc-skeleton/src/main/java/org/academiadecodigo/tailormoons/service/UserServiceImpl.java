package org.academiadecodigo.tailormoons.service;

import org.academiadecodigo.tailormoons.model.User;
import org.academiadecodigo.tailormoons.utils.Security;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private static final String DB_HOST = "jdbc:mysql://localhost:3306/ac?useTimezone=true&serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    private Connection connection;


    public UserServiceImpl() {
        try {
            connection = DriverManager.getConnection(DB_HOST, DB_USER, DB_PASSWORD);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    private PreparedStatement createStatement(String query) throws SQLException {
        return connection.prepareStatement(query);
    }


    private void closeStatement(Statement statement) throws SQLException {
        statement.close();
    }


    @Override
    public boolean authenticate(String username, String password) {
        boolean logged = false;

        try {
            String query = "SELECT password FROM user WHERE username = ?";

            PreparedStatement statement = createStatement(query);

            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String db_pw = resultSet.getString("password");
                password = Security.getHash(password);
                logged = password.equals(db_pw);
            }

            closeStatement(statement);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return logged;
    }


    @Override
    public void add(User user) {
        try {
            String username = user.getUsername();
            String email = user.getEmail();
            String password = user.getPassword();
            String firstName = user.getFirstName();
            String lastName = user.getLastName();
            String phone = user.getPhone();

            String query = "INSERT INTO user(username,email,password,firstname,lastname,phone) " +
                    "VALUES(?,?,?,?,?,?)";

            PreparedStatement statement = createStatement(query);

            statement.setString(1, username);
            statement.setString(2, email);
            statement.setString(3, password);
            statement.setString(4, firstName);
            statement.setString(5, lastName);
            statement.setString(6, phone);

            statement.executeUpdate();

            closeStatement(statement);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public User findByName(String username) {
        User user = null;

        try {
            String query = "SELECT * FROM user WHERE username = ?";

            PreparedStatement statement = createStatement(query);

            statement.setString(1, username);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }

            closeStatement(statement);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return user;
    }


    @Override
    public List<User> findAll() {
        List<User> users = new LinkedList<>();

        try {
            String query = "SELECT * FROM user";

            PreparedStatement statement = createStatement(query);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                users.add(getUserFromResultSet(resultSet));
            }

            closeStatement(statement);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return users;
    }


    @Override
    public int count() {
        int count = 0;

        try {
            String query = "SELECT COUNT(id) FROM user";

            PreparedStatement statement = createStatement(query);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            count = resultSet.getInt(1);

            closeStatement(statement);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return count;
    }


    private User getUserFromResultSet(ResultSet resultSet) {
        try {
            String username = resultSet.getString("username");
            String email = resultSet.getString("email");
            String password = resultSet.getString("password");
            String firstName = resultSet.getString("firstname");
            String lastName = resultSet.getString("lastname");
            String phone = resultSet.getString("phone");
            return new User(username, email, password, firstName, lastName, phone);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
