package org.academiadecodigo.tailormoons.rangeIterator;

import java.util.Iterator;

public class RangeIteratorLinkedList implements Iterator<Integer> {

    private Integer index = 0;
    private LinkedList<Integer> linkedList;

    public RangeIteratorLinkedList(LinkedList linkedList) {
        this.linkedList = linkedList;

    }


    @Override
    public boolean hasNext() {
        return linkedList.get(index) != null;
    }

    @Override
    public Integer next() {
        index++;
        return linkedList.get(index - 1);
    }

}
