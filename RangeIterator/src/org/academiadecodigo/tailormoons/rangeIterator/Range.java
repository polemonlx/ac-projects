package org.academiadecodigo.tailormoons.rangeIterator;

import java.util.Iterator;

public class Range implements Iterable<Integer> {

    private final int min;
    private final int max;
    private boolean isIncrementing;
    private int index;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public void setIncrementing() {
        isIncrementing = !isIncrementing;
    }

    @Override
    public Iterator<Integer> iterator() {


        index = max;
        if (isIncrementing) {
            index = min;
        }

        return new Iterator<>() {

            final boolean incrementing = isIncrementing;

            @Override
            public boolean hasNext() {
                if (incrementing) {
                    return index <= max;
                }
                return index >= min;
            }

            @Override
            public Integer next() {
                if (incrementing) {
                    index++;
                    return index - 1;
                }
                index--;
                return index + 1;
            }
        };
    }

}
