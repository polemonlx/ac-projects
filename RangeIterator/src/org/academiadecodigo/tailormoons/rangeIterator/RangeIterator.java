package org.academiadecodigo.tailormoons.rangeIterator;

import java.util.Iterator;

public class RangeIterator implements Iterator<Integer> {

    private int min;
    private int max;
    private Integer index;

    public RangeIterator(int min, int max) {
        this.min = min;
        this.max = max;
        index = min;
    }


    @Override
    public boolean hasNext() {
        return index <= max;
    }

    @Override
    public Integer next() {
        index++;
        return index-1;
    }

}
