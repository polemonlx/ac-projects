package org.academiadecodigo.tailormoons.histrogram_word;

public class Main {

    public static void main(String[] args) {

        ByComposition byComposition = new ByComposition();

        byComposition.add("ola eu sou o diogo eu sou");

        for (String key : byComposition) {
            System.out.println(key + ": " + byComposition.countWord(key));
        }

        System.out.println();

        ByInheritance byInheritance = new ByInheritance();

        byInheritance.add("ola eu sou o diogo eu sou");

        for (String key : byInheritance) {
            System.out.println(key + ": " + byInheritance.countWord(key));
        }

    }

}
