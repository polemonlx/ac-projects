package org.academiadecodigo.bootcamp.magiclamp;

public class GrumpyGenie extends Genie{


    public GrumpyGenie(int wishesMax, String name) {
        super(wishesMax, name);
    }


    @Override
    public boolean hasWishes(MagicLamp magicLamp) {
        if (super.getWishesGranted() == 1) {
            return false;
        }
        return true;
    }

}
