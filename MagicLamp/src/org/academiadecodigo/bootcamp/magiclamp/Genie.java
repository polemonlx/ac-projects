package org.academiadecodigo.bootcamp.magiclamp;

public class Genie {

    private int wishesMax;
    private int wishesGranted = 0;
    private String name;


    public Genie(int wishesMax, String name) {
        this.wishesMax = wishesMax;
        this.name = name;
    }


    public boolean grantWish(MagicLamp magicLamp) {
        if (hasWishes(magicLamp)) {
            wishesGranted++;
            return true;
        }
        return false;
    }


    public boolean hasWishes(MagicLamp magicLamp) {
        if (wishesGranted == wishesMax) {
            return false;
        }
        return true;
    }


    public int getWishesMax() {
        return wishesMax;
    }


    public int getWishesGranted() {
        return wishesGranted;
    }

}
