package org.academiadecodio.tailormoons.map_filter_reduce_stream;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        // STARTING STRING
        String message = "I'll send an SOS to the garbage world, " +
                "I hope that someone garbage gets my message in a garbage bottle.";

        // METHOD ONE
        String newMessage = Stream.of(message.split(" "))
                .filter(text -> !text.equals("garbage"))
                .map(String::toUpperCase)
                .reduce("", (accumulator, text) -> accumulator  + text + " ");

        // PRINT BOTH METHODS
        System.out.println(newMessage);

    }

}
