package org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable;

public interface Destroyable {

    void hit(int damage);

    boolean isDestroyed();

}
