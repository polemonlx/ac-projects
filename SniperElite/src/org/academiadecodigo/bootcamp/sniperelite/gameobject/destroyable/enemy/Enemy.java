package org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy;

import org.academiadecodigo.bootcamp.sniperelite.gameobject.GameObject;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.Destroyable;

public abstract class Enemy extends GameObject implements Destroyable {


    private int health = 100;
    private boolean isDead;


    public boolean isDead() {
        return isDead;
    }

    @Override
    public void hit(int damage) {
        health = Math.max(health - damage, 0);
        isDead = health == 0;
    }


    @Override
    public String getMessage() {
        return getClass().getSimpleName();
    }


    @Override
    public boolean isDestroyed() {
        return isDead;
    }

}
