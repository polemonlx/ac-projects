package org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.enemy;

public class ArmouredEnemy extends Enemy {

    private int armour = 25;


    @Override
    public void hit(int damage) {
        int damageHealth = Math.max(damage - armour, 0);
        armour = Math.max(armour - damage, 0);
        super.hit(damageHealth);
    }

    @Override
    public String toString(){
        return "I am armoured";
    }
}
