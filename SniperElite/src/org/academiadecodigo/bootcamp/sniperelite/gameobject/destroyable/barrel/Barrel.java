package org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.barrel;

import org.academiadecodigo.bootcamp.sniperelite.gameobject.GameObject;
import org.academiadecodigo.bootcamp.sniperelite.gameobject.destroyable.Destroyable;

public class Barrel extends GameObject implements Destroyable {

    private final BarrelType barrelType;
    private int currentDamage;
    private boolean destroyed;

    public Barrel(BarrelType barrelType) {
        this.barrelType = barrelType;
    }


    public void hit(int damage) {
        currentDamage = Math.min(damage + currentDamage, barrelType.getMaxDamage());
        destroyed = currentDamage == barrelType.getMaxDamage();
    }


    public boolean isDestroyed() {
        return destroyed;
    }


    @Override
    public String toString() {
        return getMessage();
    }

    @Override
    public String getMessage() {
        return "I am a barrel";
    }

}
